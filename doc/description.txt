HI Thank you for your interest. Please review the following and let me know if it can be done and what is the time and budget.

1. Customer enter premise.
2. Pick a number ticket at Touch Screen Terminal (TST-1) and go to waiting area <patient enters Registration Queue>
3. Nurse can look at the registration queue on her PC, calls the next patient when she becomes available 
4. A message # 4 Please come to Reception displayed at a small screen at Reception
5. Patient comes to Reception and completes registration, generate a bar code for the patient, that has information about the name of the patient and whether he/she has made an appointment, the appointment time, VIP status. 
6. Nurse will scan the bar code to the Queue system, then click a button on her PC to send the patient into Treatment Queue, after registration is completed.
7. Treatment Queue Logic: Three types of patient priority.
A VIP 
B Patient with booking (on time, 15 min before and 15 min after scheduled time)
C Patient with booking (early, by over 15 min)
D Patient with booking (late by over 15 min)
E Walk in Patient
Queue Priority
Priority 1 - A
Priority 2 - B
Priority 3 - C, D (Priority of C will be moved up when it is within 15 of their booking time)
Priority 4 - E
8 Doctor can see the queue and press a button on their PC when he/she is available to see the next patient
9 A message <John Au Please come to Treatment Room A> displayed at a small screen at the display in the clinic. 
10 John Au enter treatment room A and see the doctor. A pop up screen will show on Doctor's screen, Doctor can stop it when John Au shows up. If John Au doesn't show iin 3 min, the system will call the next patient in queue to come to treatment room A, until a patient shows up. 
10 John Au will be passed to the Drug Dispensary Queue when the doctor clicks finish, or when the doctor calls the next patient into treatment room A.
11 John Au waits for drug dispensary and his bill. 
12 Nurse at reception has prepared John's drug. Press the button on her PC at reception to call John Au to reception
13 A message <John Au Please come to reception> displayed at a small screen at the display in the clinic.
14 John Au goes to reception. A pop up screen will flash on nurse screen when John is being called. Nurse can select Arrived (when John Au shows at Reception) or choose to choose not available for John Au. Nurse may decide to call the next patient in queue to serve. 
15 When nurse becomes available again, she may call John Au again and repeat the process.
16 Nurse should be able to remove patient in queue or change patient priority when necessary 
17 Nurse will click completed for John Au, after he has collected drugs and settled his payment.